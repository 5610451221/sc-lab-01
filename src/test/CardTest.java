package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import test.CardTest.ListenerMgr;
import model.Card;
import gui.CardFrame;

public class CardTest {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new CardTest();
		
	}

	public CardTest() {
		frame = new CardFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(600, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		Card stu1 = new Card(0,"May",1);
        frame.setResult(stu1.toString());
        topping(stu1,300);
        frame.extendResult("Add Money :" + "  " + "300.0");
        frame.extendResult("Balance : "+ "   " +stu1.getMoney()+" ");
		frame.extendResult("Price :" + " " + "50.0");
		paying(stu1,50);
		frame.extendResult("Balance : "+ "   " +stu1.getMoney()+" ");
		
		
		
		Card stu2 = new Card(100,"Phuak",2);
		frame.extendResult(stu2.toString());
        topping(stu2,200);
        frame.extendResult("Add Money :" + "  " + "200.0");
        frame.extendResult("Balance : "+ "   " +stu2.getMoney()+" ");
        frame.extendResult("Price :" + " " + "120.0");
		paying(stu2,120);
		frame.extendResult("Balance : "+ "   " +stu2.getMoney()+" ");
		
		
	}

	ActionListener list;
	CardFrame frame;
	
	public double topping(Card card,double money){
		double amount;
		amount = card.getMoney();
		amount = amount + money;
		card.setMoney(amount);
		return amount;
	}
	
	
	public double paying(Card card,double money){
		double amount;
		amount = card.getMoney();
		amount = amount - money;
		card.setMoney(amount);
		return amount;
		
	}
	
}

