package model;

public class Card {
	private double money;
	private String name;
	private int id;

	public Card(double money,String name,int id){
		this.money = money;
		this.name = name;
		this.id = id;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String toString() {
		return id + "  " + name;
	}
	
	

}	